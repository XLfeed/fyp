﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingTexture : MonoBehaviour
{
    public float xScroll = 0.1f;
    public float yScroll = 0.1f;

    private Renderer rend;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float OffsetX = Time.deltaTime * xScroll;
        float OffsetY = Time.deltaTime * yScroll;
        rend.material.mainTextureOffset += new Vector2(OffsetX, OffsetY);
    }
}
