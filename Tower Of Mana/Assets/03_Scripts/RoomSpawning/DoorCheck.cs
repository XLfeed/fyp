﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCheck : MonoBehaviour
{
    public GameObject TpPoint; //to teleport player (not in use now)
    public Transform DoorPosition; //door position (transform)
    public Door DoorReference;

    public BaseRoom ParentRoom; //getting the parent room/room its on
    private RoomInfo roomInfo;
    private GameManager manager; //referencing gamemanager

    public DoorPosition marker; //door position script

    //runs before start
    private void Awake()
    {
        manager = FindObjectOfType<GameManager>(); //get manager
        StartCoroutine(SetParent()); //getting parent room
    }

    public void PassRoom(BaseRoom _baseRoom)
    {
        ParentRoom = _baseRoom; //making sure the parentroom info is passed properly
    }

    public void EnterRoom()
    {
        Debug.Log("Entered Room (" + roomInfo.x + ", " + roomInfo.y + ")");
        GameManager.instance.EnterRoom(TpPoint, ParentRoom);
    }

    IEnumerator SetParent() //setting the parent room
    {
        yield return new WaitForSeconds(1f);
        BaseRoom[] baseRooms = FindObjectsOfType<BaseRoom>();
        for (int i = 0; i < baseRooms.Length; i++)
        {
            if (baseRooms[i].x == marker.x && baseRooms[i].y == marker.y)
            {
                ParentRoom = baseRooms[i];
            }
        }

        if(ParentRoom != null)
        {
            ParentRoom.PassDoor(DoorReference);
            roomInfo = ParentRoom.GetRoomInfo();
            DoorReference.SetUp(ParentRoom, this);
        }
    }
}
