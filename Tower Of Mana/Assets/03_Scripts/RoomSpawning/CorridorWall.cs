﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorWall : MonoBehaviour
{
    public GameObject RightWallHolder;
    public GameObject LeftWallHolder;

    private RoomsGenerator generator;

    public void GetGenerator(RoomsGenerator _generator)
    {
        generator = _generator;

        int randNum = Random.Range(0, generator.CorridorWalls.Length);
        GameObject RightWall = Instantiate(generator.CorridorWalls[randNum], RightWallHolder.transform.position, RightWallHolder.transform.rotation, RightWallHolder.transform);

        randNum = Random.Range(0, generator.CorridorWalls.Length);
        GameObject LeftWall = Instantiate(generator.CorridorWalls[randNum], LeftWallHolder.transform.position, LeftWallHolder.transform.rotation, LeftWallHolder.transform);
    }
}
