﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class RoomInfo : MonoBehaviour
{
    //room coordinates in giant grid
    public int x;
    public int y;

    //these ints will change if wall/door available
    public int top = 1;
    public int bottom = 1;
    public int right = 1;
    public int left = 1;

    //what room type
    public bool startRoom = false;
    public bool endRoom = false;
    public bool shopRoom = false;

    //has the room been visited
    public bool visited;

    private RoomsGenerator generator;

    public void SetPosition(int _x, int _y, RoomsGenerator _generator) //setting the position in grid
    {
        generator = _generator;

        x = _x;
        y = _y;

        if (x == 0)
            left = 0;
        if (y == 0)
            bottom = 0;
        if (x == generator.roomRows - 1)
            right = 0;
        if (y == generator.roomColumns - 1)
            top = 0;
    }

    public void SetStartRoom() //setting start room, look at the name
    {
        startRoom = true;
        visited = true;

        if (x == 0)
            left = 3;
        if (y == 0)
            bottom = 3;
        if (x == generator.roomRows - 1)
            right = 3;
        if (y == generator.roomColumns - 1)
            top = 3;
    }
    //'direction' = 0 (Wall)
    //'direction' = 1 (Available Path)
    //'direction' = 2 (Door + Corridor)
    //'direction' = 3 (Start Door)

    public void SetEndRoom() //set end room, its in the name
    {
        endRoom = true;
        visited = true;
    }

    public void SetShopRoom() //set shop room, its in the name
    {
        shopRoom = true;
        visited = true;
    }

    public void SetVisited() //look at name, checking if player visited the room
    {
        visited = true;
    }

    public void CurrSetDoor(int direction) //setting the door direction
    {
        if (direction == 0)
            top = 2;
        if (direction == 1)
            right = 2;
        if (direction == 2)
            bottom = 2;
        if (direction == 3)
            left = 2;
    }
    public void DestSetDoor(int direction) 
    {
        if (direction == 0)
            bottom = 2;
        if (direction == 1)
            left = 2;
        if (direction == 2)
            top = 2;
        if (direction == 3)
            right = 2;
    }

    public void SpawnRoom(GameObject roomBase) //spawning the actual room prefab
    {
        GameObject BasRoom = Instantiate(roomBase, transform.position, transform.rotation, gameObject.transform);
        BaseRoom inBaseRoom = BasRoom.GetComponent<BaseRoom>();

        inBaseRoom.GenerateRoom(this, generator);
        inBaseRoom.PassPos(this);
    }

    public void emptySpot(GameObject FillerSpot)
    {
        GameObject _FillerSpot = Instantiate(FillerSpot, transform.position, transform.rotation, gameObject.transform);
    }
}
