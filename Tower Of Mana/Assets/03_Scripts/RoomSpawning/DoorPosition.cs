﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPosition : MonoBehaviour
{
    public int x;
    public int y;

    public void PassPos(RoomInfo _roomInfo)
    {
        x = _roomInfo.x;
        y = _roomInfo.y;
    }
}
