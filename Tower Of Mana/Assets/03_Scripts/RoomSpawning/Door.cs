﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Collider collider;
    public MeshRenderer renderer;

    private BaseRoom baseRoom;
    private DoorCheck doorCheck;

    private void Awake()
    {
        collider.enabled = true;
        renderer.enabled = false;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            baseRoom.EnterRoom();
            doorCheck.EnterRoom();
        }
    }

    public void SetUp(BaseRoom _baseRoom, DoorCheck _doorCheck)
    {
        baseRoom = _baseRoom;
        doorCheck = _doorCheck;
    }
}
