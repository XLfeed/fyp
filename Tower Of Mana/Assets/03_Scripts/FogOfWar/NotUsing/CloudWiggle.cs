﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudWiggle : MonoBehaviour
{
    public float maxSize;
    public float minSize;
    public float maxScaleSpeed;
    public float minScaleSpeed;

    private float desireSize;
    private float desireSpeed;

    //private MeshRenderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        //renderer = GetComponent<MeshRenderer>();

        GenerateNewSize();
    }

    // Update is called once per frame
    void Update()
    {
        if(desireSize < transform.localScale.x)
        {
            transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * desireSpeed, transform.localScale.y - Time.deltaTime * desireSpeed, transform.localScale.z - Time.deltaTime * desireSpeed);

            if(transform.localScale.x <= desireSize)
            {
                GenerateNewSize();
            }
        }
        else if (desireSize > transform.localScale.x)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * desireSpeed, transform.localScale.y + Time.deltaTime * desireSpeed, transform.localScale.z + Time.deltaTime * desireSpeed);

            if (transform.localScale.x >= desireSize)
            {
                GenerateNewSize();
            }
        }

        /*Color currentColor = renderer.material.color;
        currentColor.a = transform.localScale.x;
        Mathf.Clamp01(currentColor.a);
        renderer.material.color = currentColor;*/
    }

    private void GenerateNewSize()
    {
        desireSize = Random.Range(minSize, maxSize);
        desireSpeed = Random.Range(minScaleSpeed, maxScaleSpeed);
    }
}
