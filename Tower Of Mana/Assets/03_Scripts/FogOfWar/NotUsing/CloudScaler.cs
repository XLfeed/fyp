﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScaler : MonoBehaviour
{
    public float scaleSpeed;
    public float maxSize;
    private int scalingDirection = 0;
    private bool doScaling = true;
    private bool scalable = true;

    // Update is called once per frame
    void Update()
    {
        if (doScaling)
        {
            if (scalingDirection == 0)
            {
                Vector3 desiredSize = new Vector3(transform.localScale.x - Time.deltaTime * scaleSpeed, transform.localScale.y - Time.deltaTime * scaleSpeed, transform.localScale.z - Time.deltaTime * scaleSpeed);
                transform.localScale = desiredSize;

                if(transform.localScale.x <= 0.1)
                {
                    doScaling = false;
                    gameObject.SetActive(false);
                }
            }
            else if (scalingDirection == 1)
            {
                Vector3 desiredSize = new Vector3(transform.localScale.x + Time.deltaTime * scaleSpeed, transform.localScale.y + Time.deltaTime * scaleSpeed, transform.localScale.z + Time.deltaTime * scaleSpeed);
                transform.localScale = desiredSize;

                if (transform.localScale.x >= maxSize)
                {
                    transform.localScale = new Vector3(maxSize, maxSize, maxSize);
                    doScaling = false;
                }
            }
            if (scalingDirection == -1)
            {
                scalable = false;
                Vector3 desiredSize = new Vector3(transform.localScale.x - Time.deltaTime * scaleSpeed, transform.localScale.y - Time.deltaTime * scaleSpeed, transform.localScale.z - Time.deltaTime * scaleSpeed);
                transform.localScale = desiredSize;

                if (transform.localScale.x <= 0.1)
                {
                    doScaling = false;
                    gameObject.SetActive(false);
                }
            }
        }
    }

    public void ToggleScale(int _scaleDir)
    {
        scalingDirection = _scaleDir;
        doScaling = true;
    }

    public bool GetScalable()
    {
        return scalable;
    }
}
