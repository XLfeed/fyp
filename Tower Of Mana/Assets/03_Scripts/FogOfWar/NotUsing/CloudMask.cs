﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMask : MonoBehaviour
{
    private GameObject eyeOfStorm;
    public Vector3 maskOffset;
    public Vector3 roomMaskOffset;
    public float maskDistance;
    public float maxDistance;
    public float roomMask;
    public bool show = true;
    public bool scale = false;
    private int updateCounter = 0;

    public static CloudMask instance;

    private List<BaseRoom> Explored;

    private List<float> distances;

    private void Awake()
    {
        instance = this;
        Explored = new List<BaseRoom>();

        distances = new List<float>();
    }

    public void AddRoom(BaseRoom _room)
    {
        Explored.Add(_room);
    }

    public void GetPlayer(GameObject _player)
    {
        eyeOfStorm = _player;
    }

    // Update is called once per frame
    void Update()
    {
        if(eyeOfStorm == null)
            return;
        if(GameManager.instance.GetEnemyCount() <= 0)
            UpdateMask();
    }

    void UpdateMask()
    {
        if(updateCounter <= 0)
            updateCounter++;

        foreach (Transform child in CloudGenerator.instance.transform)
        {
            CloudScaler scaler = child.GetComponent<CloudScaler>();

            bool updateCheck = scaler.GetScalable();
            if (!updateCheck)
            {
                continue;
            }

            float distance = Vector3.Distance(child.position, eyeOfStorm.transform.position + maskOffset);

            foreach (BaseRoom room in Explored)
            {
                float dist = Vector3.Distance(child.position, room.gameObject.transform.position + roomMaskOffset);
                distances.Add(dist);
            }

            if(updateCounter >= 1)
            {
                foreach (float dist in distances)
                {
                    if (dist < roomMask)
                    {
                        scaler.ToggleScale(-1);
                    }
                }
            }

            if (distance < maskDistance)
            {
                if (!show)
                {
                    if (!child.gameObject.activeInHierarchy)
                    {
                        child.gameObject.SetActive(true);
                    }
                }
                else
                {
                    if (child.gameObject.activeInHierarchy && !scale)
                    {
                        child.gameObject.SetActive(false);
                    }
                    else if (child.gameObject.activeInHierarchy && scale)
                    {
                        scaler.ToggleScale(0);
                    }
                }
            }
            else
            {
                if (!show)
                {
                    if (child.gameObject.activeInHierarchy)
                    {
                        child.gameObject.SetActive(false);
                    }
                }
                else
                {
                    if (distance < maxDistance)
                    {
                        if (!child.gameObject.activeInHierarchy && !scale)
                        {
                            child.gameObject.SetActive(true);
                        }
                        else if (!child.gameObject.activeInHierarchy && scale)
                        {
                            child.gameObject.SetActive(true);
                            if (scaler.GetScalable())
                            {
                                scaler.ToggleScale(1);
                            }
                        }
                    }
                    else
                    {
                        child.gameObject.SetActive(false);
                    }
                }
            }
            distances.Clear();
        }
    }
}
