﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{
    public GameObject Cloud;
    public float width;
    public float height;
    public float intDistance;
    public float heightOffset;

    public static CloudGenerator instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject _cloud = Instantiate(Cloud, new Vector3(transform.position.x + x * intDistance, transform.position.y + heightOffset, transform.position.z + y * intDistance), Quaternion.identity, transform);

                _cloud.name = "Cloud_" + x + "_" + y;
            }
        }
    }
}
