﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class FogGenerator : MonoBehaviour
{
    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;

    [Header("Fog Size Settings")]
    public int xSize = 20;
    public int zSize = 20;

    [Header("Fog Mask Settings")]
    public GameObject FogOfWarPlane;
    private Transform player;
    public LayerMask fogMask;
    public float maskRadius = 5f;
    public float roomRadius;
    public Camera mainCam;

    private MeshCollider collider;
    private Vector3[] maskVertices;
    private Color[] maskColors;

    [Header("Fog Fluctuation Settings")]
    public float fogX;
    public float fogZ;
    public float fogHeight;
    public float randScale;
    public float randOffsetX;
    public float randOffsetZ;

    [Header("Fog Thickness Settings")]
    public float noiseScale;
    public float noiseOffsetX;
    public float noiseOffsetZ;

    [Header("Fog Flow Settings")]
    public float maxWindSpeed;
    public float minWindSpeed;
    public float maxTime;
    public float minTime;

    private float windTimer = 0;
    private Vector2 windDirection;

    private List<BaseRoom> ExploredRooms;
    private BaseRoom currentRoom;
    private bool inCombat = false;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        collider = GetComponent<MeshCollider>();
        collider.sharedMesh = mesh;

        CreateShape();
        UpdateMesh();

        Initialize();

        ExploredRooms = new List<BaseRoom>();

        float rand1 = Random.Range(minWindSpeed, maxWindSpeed);
        float rand2 = Random.Range(minWindSpeed, maxWindSpeed);

        windDirection = new Vector2(rand1, rand2);
    }

    private void Update()
    {
        if (FogOfWarPlane == null)
            return;

        noiseOffsetX += Time.deltaTime * windDirection.x;
        noiseOffsetZ += Time.deltaTime * windDirection.y;

        if (GameManager.instance.Player != null)
        {
            player = GameManager.instance.Player.transform;
            transform.position = new Vector3(fogX, fogHeight, fogZ);
        }

        if (!inCombat)
        {
            Ray ray = new Ray(mainCam.transform.position, player.position - mainCam.transform.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, fogMask, QueryTriggerInteraction.Collide))
            {
                for (int i = 0; i < maskVertices.Length; i++)
                {
                    Vector3 vertexPoint = FogOfWarPlane.transform.TransformPoint(maskVertices[i]);
                    float dist = Vector3.SqrMagnitude(vertexPoint - hit.point);

                    if (dist < (maskRadius * maskRadius))
                    {
                        float alpha = Mathf.Min(maskColors[i].a, dist / (maskRadius * maskRadius));
                        maskColors[i].a = alpha;
                    }
                    else
                    {
                        float xCoord = vertexPoint.x / xSize * noiseScale + noiseOffsetX;
                        float zCoord = vertexPoint.z / zSize * noiseScale + noiseOffsetZ;

                        float alpha = Mathf.PerlinNoise(xCoord, zCoord);
                        maskColors[i].a = alpha;
                    }
                }

                if (ExploredRooms.Count >= 1)
                {
                    foreach (BaseRoom baseRoom in ExploredRooms)
                    {
                        Ray r = new Ray(mainCam.transform.position, baseRoom.gameObject.transform.position - mainCam.transform.position);
                        RaycastHit h;
                        if (Physics.Raycast(r, out h, 1000, fogMask, QueryTriggerInteraction.Collide))
                        {
                            for (int i = 0; i < maskVertices.Length; i++)
                            {
                                Vector3 vertexPoint = FogOfWarPlane.transform.TransformPoint(maskVertices[i]);
                                float dist = Vector3.SqrMagnitude(vertexPoint - h.point);

                                if (dist < (roomRadius * roomRadius))
                                {
                                    float alpha = Mathf.Min(maskColors[i].a, dist / (roomRadius * roomRadius));
                                    maskColors[i].a = alpha;
                                }
                            }
                        }
                    }
                }
                UpdateColor();
            }
        }
        else
        {
            if (currentRoom != null)
            {
                Ray r = new Ray(mainCam.transform.position, currentRoom.gameObject.transform.position - mainCam.transform.position);
                RaycastHit h;
                if (Physics.Raycast(r, out h, 1000, fogMask, QueryTriggerInteraction.Collide))
                {
                    for (int i = 0; i < maskVertices.Length; i++)
                    {
                        Vector3 vertexPoint = FogOfWarPlane.transform.TransformPoint(maskVertices[i]);
                        float dist = Vector3.SqrMagnitude(vertexPoint - h.point);

                        if (dist < (roomRadius * roomRadius))
                        {
                            float alpha = Mathf.Min(maskColors[i].a, dist / (roomRadius * roomRadius));
                            maskColors[i].a = alpha;
                        }
                        else
                        {
                            float xCoord = vertexPoint.x / xSize * noiseScale + noiseOffsetX;
                            float zCoord = vertexPoint.z / zSize * noiseScale + noiseOffsetZ;

                            float alpha = Mathf.PerlinNoise(xCoord, zCoord);
                            maskColors[i].a = alpha;
                        }
                    }
                }
            }
            UpdateColor();
        }
    }

    void CreateShape()
    {
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];

        for (int i = 0, z = 0; z <= zSize; z++) 
        {
            for (int x = 0; x <= xSize; x++)
            {
                float xCoord = (float)x / xSize * randScale + randOffsetX;
                float zCoord = (float)z / zSize * randScale + randOffsetZ;

                float y = Mathf.PerlinNoise(xCoord, zCoord);
                vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }


        triangles = new int[xSize * zSize * 6];
        int vert = 0;
        int tris = 0;

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }
    }

    void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
        
    }

    void Initialize()
    {
        maskVertices = mesh.vertices;
        maskColors = new Color[maskVertices.Length];

        for (int i = 0; i < maskColors.Length; i++)
        {
            maskColors[i] = Color.white;
        }

        UpdateColor();
    }

    void UpdateColor()
    {
        mesh.colors = maskColors;
    }

    public void AddRoom(BaseRoom _room)
    {
        ExploredRooms.Add(_room);
    }

    public void GetCurrentRoom(BaseRoom _currentRoom)
    {
        currentRoom = _currentRoom;
        inCombat = true;
    }
    public void OutOfCombat()
    {
        inCombat = false;
        currentRoom = null;
    }
}
