﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject PauseScreen;
    public static bool gamePaused = false;
    public GameObject OptionScreen;
    

    [Header("Audio")]
    public AudioSource buttonAudio;
    public AudioSource hoverAudio;
    public AudioClip buttonSFX;
    public AudioClip hoverSFX;

    public GameObject restartArrow;
    public GameObject mainMenuArrow;
    public GameObject loseRestartArrow;
    public GameObject loseMainMenuArrow;
    public GameObject pauseRestartArrow;
    public GameObject pauseMainMenuArrow;
    public GameObject pauseOptionArrow;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!gamePaused)
            {               
                PauseGame();
            }
            else
            {
                ResumeGame();
            }
        }
    }
    public void Restart()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
        
        PlayerPrefs.SetFloat("CurrentHealth", 0);
        PlayerPrefs.SetInt("HealthPotionCount", 0);
        PlayerPrefs.SetInt("CoinCount", 0);
    }

    public void MainMenu()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        SceneManager.LoadScene(0);
    }

    public void PauseGame()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        gamePaused = true;
        PauseScreen.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        gamePaused = false;
        PauseScreen.SetActive(false);
        OptionScreen.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Options()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        OptionScreen.SetActive(true);
    }

    public void BackToPause()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        OptionScreen.SetActive(false);
    }

    public void RestartHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        restartArrow.SetActive(true);
    }

    public void MainMenuHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        mainMenuArrow.SetActive(true);
    }

    public void LoseRestartHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        loseRestartArrow.SetActive(true);
    }

    public void LoseMainMenuHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        loseMainMenuArrow.SetActive(true);
    }

    public void PauseMainMenuHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        pauseMainMenuArrow.SetActive(true);
    }

    public void PauseRestartHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        pauseRestartArrow.SetActive(true);
    }

    public void PasueOptionHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        pauseOptionArrow.SetActive(true);
    }

    public void NoHover()
    {
        restartArrow.SetActive(false);
        mainMenuArrow.SetActive(false);
        loseRestartArrow.SetActive(false);
        loseMainMenuArrow.SetActive(false);
        pauseMainMenuArrow.SetActive(false);
        pauseRestartArrow.SetActive(false);
        pauseOptionArrow.SetActive(false);
    }

    public void returnHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
    }
}
