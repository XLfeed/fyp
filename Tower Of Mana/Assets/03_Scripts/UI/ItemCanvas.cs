﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCanvas : MonoBehaviour
{
    Camera cam; 
    void Start()
    {
        cam = GameManager.instance.Camera;
    }

    void Update()
    {
        transform.rotation = cam.transform.rotation * Quaternion.Euler(Vector3.up * 180);
    }
}
