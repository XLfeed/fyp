﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{

    private static readonly string FirstPlay = "FirstPlay";     // Default values when playing for the first time
    private static readonly string bgmPref = "bgmPref";         // Background music
    private static readonly string sfxPref = "sfxPref";         // Sound effects
    private static readonly string ambPref = "ambPref";         // Ambience sound

    private int firstPlayInt;

    [Header("Sliders")]
    public Slider bgmSlider, sfxSlider, ambSlider;
    private float bgmVol, sfxVol, ambVol;

    [Header("AudioSources")]
    public AudioSource bgmAudio;
    public AudioSource[] sfxAudio;                              // To store all sfx
    public AudioSource[] ambAudio;



    // Start is called before the first frame update
    void Start()
    {
        firstPlayInt = PlayerPrefs.GetInt(FirstPlay);

        if(firstPlayInt == 0)                         // by deafult playerprefs = 0
        {
            // Setting dafult volumes
            bgmVol = .5f;
            bgmSlider.value = bgmVol;
            sfxVol = 1f;
            sfxSlider.value = sfxVol;
            ambVol = 1f;
            ambSlider.value = ambVol;

            // Save values throughout gameplay
            PlayerPrefs.SetFloat(bgmPref, bgmVol);
            PlayerPrefs.SetFloat(sfxPref, sfxVol);
            PlayerPrefs.SetFloat(ambPref, ambVol);

            PlayerPrefs.SetInt(FirstPlay, -1);        // End
        }
        else
        {
            bgmVol = PlayerPrefs.GetFloat(bgmPref);
            bgmSlider.value = bgmVol;
            sfxVol = PlayerPrefs.GetFloat(sfxPref);
            sfxSlider.value = sfxVol;
            ambVol = PlayerPrefs.GetFloat(ambPref);
            ambSlider.value = ambVol;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveSoundSettings()                   // Get actual slider values 
    {
        PlayerPrefs.SetFloat(bgmPref, bgmSlider.value);
        PlayerPrefs.SetFloat(sfxPref, sfxSlider.value);
        PlayerPrefs.SetFloat(ambPref, ambSlider.value);
    }

    private void OnApplicationFocus(bool inFocus)     // If exit game, values will still be saved(focus in game!)
    {
        if (!inFocus)
        {
            SaveSoundSettings();
        }
    }

    // Updates Sound when slider is used in game
    public void UpdateSound()
    {
        bgmAudio.volume = bgmSlider.value;
        
        // Allows many sounds
        for(int i = 0; i < sfxAudio.Length; i++)
        {
            sfxAudio[i].volume = sfxSlider.value;
        }

        for (int i = 0; i < ambAudio.Length; i++)
        {
            ambAudio[i].volume = ambSlider.value;
        }
    }
}
