﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is for audio to be carried over different scenes
public class AudioSettings : MonoBehaviour
{
    private static readonly string bgmPref = "bgmPref";         // Background music
    private static readonly string sfxPref = "sfxPref";         // Sound effects
    private static readonly string ambPref = "ambPref";         // Ambience sound

    private float bgmVol, sfxVol, ambVol;

    [Header("AudioSources")]
    public AudioSource bgmAudio;
    public AudioSource[] sfxAudio;                              // To store all sfx
    public AudioSource[] ambAudio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        ContinueSettings();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ContinueSettings()
    {
        bgmVol = PlayerPrefs.GetFloat(bgmPref);
        sfxVol = PlayerPrefs.GetFloat(sfxPref);
        ambVol = PlayerPrefs.GetFloat(ambPref);

        bgmAudio.volume = bgmVol;

        // Allows many sounds
        for (int i = 0; i < sfxAudio.Length; i++)
        {
            sfxAudio[i].volume = sfxVol;
        }

        for (int i = 0; i < ambAudio.Length; i++)
        {
            ambAudio[i].volume = ambVol;
        }
    }
}
