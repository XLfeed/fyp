﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelect : MonoBehaviour
{
    [Header("SelectingEnemy")]
    [SerializeField] string Enemy = "Enemy";        // Only applies to enemy
    bool targeted = false;
    private Transform selected;
    private Transform theSelectedEnemy;

    [SerializeField] LayerMask enemyMask;                   // layer mask to tell our ray which layer to hit, i.e. allows ray to hit objects with ENEMY LAYER through fog mesh;


    // Start is called before the first frame update
    void Start()
    {
        // gameMaster = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        HighlightSelection();
 
    }

    // To select specific enemy
    void HighlightSelection()
    {
        if (selected != null)
        {
            selected.gameObject.transform.GetChild(2).gameObject.SetActive(false);
            selected.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            // var selectionRenderer = selected.GetComponentInChildren<Renderer>();
            // selectionRenderer.material = defaultMaterial;                            // Change to default material when not selected
            selected = null;            
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);                 // Selected based on mouse position
        //Debug.DrawRay(ray.origin, ray.direction * 1000, Color.white);              // draws debug ray in scene view
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, enemyMask))                  
        {
            var selection = hit.transform;        // enemy currently highlighted

            var targetRing = selection.gameObject.transform.GetChild(2).gameObject;   // Reference to enemy's disabled target ring
            // var targetCanvas = selection.gameObject.transform.GetChild(1).gameObject; // Reference to enemy's Canvas UI
            // var selectionRenderer = selection.GetComponentInChildren<Renderer>();
            if (selection != null)
            {
                // selectionRenderer.material = highlightedMaterial;                  // Change to highlighted material/colour
                targetRing.SetActive(true);
                // targetCanvas.SetActive(true);
                
                
                // var targetRing = selection.gameObject.transform.GetChild(2).gameObject;   // Reference to enemy's disabled target ring
                var targetCanvas = selection.gameObject.transform.GetChild(1).gameObject; // Reference to enemy's Canvas UI
                
                if (Input.GetMouseButtonDown(0))                                          // If click on enemy when highlighted, target ring appears
                {

                    if (targeted == false)                                                // Enable target ring under enemy
                    {
                        // targetRing.SetActive(true);
                        targetCanvas.SetActive(true);
                        targeted = true;
                        theSelectedEnemy = selection;
                    }

                   
                    else if (targeted == true)                                            // Disable target ring around enemy
                    {
                        if (selection != theSelectedEnemy && theSelectedEnemy != null)
                        {
                            theSelectedEnemy.gameObject.transform.GetChild(2).gameObject.SetActive(false);
                            theSelectedEnemy.gameObject.transform.GetChild(1).gameObject.SetActive(false);
                            targetRing.SetActive(true);
                            targetCanvas.SetActive(true);
                            targeted = true;
                            theSelectedEnemy = selection;
                        }

                        else
                        {
                            // theSelectedEnemy = GameObject.FindWithTag("Enemy");                           
                            targetRing.SetActive(false);
                            targetCanvas.SetActive(false);
                            targeted = false;
                        }
                    }
                }
                
                selected = selection;                
            }          
        }
    }
}
