﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;

public class MainMenu : MonoBehaviour
{
    public AudioSource buttonAudio;
    public AudioClip buttonSFX;
    public AudioSource hoverAudio;
    public AudioClip hoverSFX;
    public GameObject startButton;
    
    public GameObject gradientObject;

    public GameObject playArrow;
    public GameObject creditsArrow;
    public GameObject quitArrow;

    // Purposely adding a delay just so I can hear the button sound ._. (but need double click lul)
    private float delayBeforeLoading = 1f;
    private float timeElapsed;

    public void Start()
    {
        Time.timeScale = 1;
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
    }

    //starting the game
    public void Startgame()
    {
        buttonAudio.PlayOneShot(buttonSFX);  // Play button sound
        SceneManager.LoadScene(1);           //loads testing level scene

        PlayerPrefs.SetFloat("CurrentHealth", 0);
        PlayerPrefs.SetInt("HealthPotionCount", 0);
        PlayerPrefs.SetInt("CoinCount", 0);
    }

    //credits
    public void Credits()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        if (timeElapsed > delayBeforeLoading)
        {
            
            SceneManager.LoadScene(4);
        }
        
    }

    public void QuitGame()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        PlayerPrefs.SetFloat("CurrentHealth", 0);
        PlayerPrefs.SetInt("HealthPotionCount", 0);
        PlayerPrefs.SetInt("CoinCount", 0);

        Application.Quit();
    }

    public void LoadMainMenu()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        SceneManager.LoadScene(0);
    }

    public void StartButton()
    {
        buttonAudio.PlayOneShot(buttonSFX);
        startButton.SetActive(false);        
        gradientObject.SetActive(true);
    }

    public void playHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        playArrow.SetActive(true);
    }

    public void creditsHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        creditsArrow.SetActive(true);
    }

    public void quitHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
        quitArrow.SetActive(true);
    }

    public void noHover()
    {
        playArrow.SetActive(false);
        creditsArrow.SetActive(false);
        quitArrow.SetActive(false);
    }

    public void returnHover()
    {
        buttonAudio.PlayOneShot(hoverSFX);
    }
}
