﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarCanvas : MonoBehaviour
{

    public Camera mc;

    void Start()
    {
        mc = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // HP Bar will face Camera at all times even when enemy rotates
        transform.LookAt(transform.position + mc.transform.rotation * Vector3.back, mc.transform.rotation * Vector3.up);
    }
}
