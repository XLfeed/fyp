﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stalker : MonoBehaviour
{
    private GameManager gameMaster;
    private GameObject Player;

    // Start is called before the first frame update
    void Awake()
    {
        gameMaster = FindObjectOfType<GameManager>();
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Player = gameMaster.Player;
        if (Player != null)
        {
            transform.position = Player.transform.position;
        }
    }
}
