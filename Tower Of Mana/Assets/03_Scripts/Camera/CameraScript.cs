﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{   
    [Header("Position References")]
    private Stalker Playerstalk;
    private Vector3 targetPos;

    [Header("Camera Follow Values")]
    public float LerpSpeed;
    public Vector3 CameraOffSet;
    public Vector3 RotationOffSet;

    //[Header("Camera Obstruct")]
    //Transform player;
    //public Transform obst;
    //float zoomSpeed = 2f;
    //public LayerMask layerMask;

    void Start()
    {
        Playerstalk = FindObjectOfType<Stalker>();
        //player = GameManager.instance.Player.transform;
    }

    void Update()
    {
        CameraFollow();
        //ViewObstructed();
    }

    void CameraFollow()
    {
        targetPos = Playerstalk.transform.position;                                 // Set Camera position = to Player position
        Vector3 lerpPos = Vector3.Lerp(transform.position, targetPos, LerpSpeed);   // Smooth Follow
        transform.position = lerpPos + CameraOffSet;                                // Offset Camera position
        transform.rotation = Quaternion.Euler(RotationOffSet);                      // Offset Camera rotaion
    }

    //void ViewObstructed()
    //{
    //    RaycastHit hit;
    //    Ray ray = new Ray(transform.position, player.position - transform.position);
    //    Debug.DrawRay(transform.position, (player.position - transform.position) * 1000, Color.green);

    //    if(Physics.Raycast(transform.position, transform.position - player.position, out hit, Mathf.Infinity, layerMask))
    //    {
    //        Debug.DrawRay(transform.position, (transform.position - player.position) * Mathf.Infinity, Color.red);
    //        if(hit.collider.gameObject.tag != "Player")
    //        {
    //            Debug.Log("HELLO");
    //            obst = hit.transform;
    //            MeshRenderer[] meshRenderers = obst.GetComponentsInChildren<MeshRenderer>();

    //            foreach (MeshRenderer meshrenderer in meshRenderers)
    //            {
    //                meshrenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
    //            }
    //            //obst.gameObject.GetComponent<MeshRenderer>().
    //        }
    //    }
    //}
}
