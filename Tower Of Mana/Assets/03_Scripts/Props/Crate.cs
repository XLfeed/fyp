﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour
{
    public void Explode()
    {
        //particles

        //roll a random drop
        int randNum = Random.Range(0, GameManager.instance.crateDrops.Length);

        //spawn drop
        Instantiate(GameManager.instance.crateDrops[randNum], transform.position + Vector3.up, Quaternion.identity);

        //destroy
        Destroy(gameObject);
    }
}
