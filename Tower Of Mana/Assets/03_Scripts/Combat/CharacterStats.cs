﻿using System;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
   
    // individual base class stats for enemy, mobs, bosses 
   
    public float maxHealth;
    public float currentHealth { get; protected set; }         //allows any other class to get value but only lets value be modified within this class                    
    // public GameObject popUpDamage;

    public Stat damage;                                      //damage value of current character
    public Stat armor;                                       //armor value of current character

    public event Action<float> OnHealthPercentChanged = delegate { }; // To be called whenever hp goes down (Used in Enemy Controller script's for enemy's healthbar)

    public virtual void Start()                                        //virtual Start function (Make sure to call public override void Start() {base.Start(); })
    {
        currentHealth = maxHealth;                                 //sets current health value
    }

    public void TakeDamage (float damage)
    {
        damage -= armor.GetValue();                                     //passive damage reduction by any armor
        damage = Mathf.Clamp(damage, 0, int.MaxValue);                  //if armor value > than damage taken, clamp damage recieved to 0 instead of having a lifesteal effect

        currentHealth -= damage;                                        //recieve damage
        if (gameObject.tag == "Player")
        {
            PlayerPrefs.SetFloat("CurrentHealth", currentHealth);
        }
        Debug.Log(transform.name + " takes " + damage + " damage.");

        float currentHealthPercent = currentHealth / maxHealth;         // Get current health percentage as a float
        OnHealthPercentChanged(currentHealthPercent);                   // Call "OnHealthPercentChanged: event (Enemy Controller script)

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    

    public void HealthPotion (float hpIncrease)
    {
        currentHealth = Mathf.Lerp(currentHealth, currentHealth + hpIncrease, 2000f* Time.deltaTime);
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
    }

    public virtual void Die()                                            //to be overridden for specific characters e.g. player, mobs, boss
    {
        Debug.Log(transform.name + " has died.");                        
    }
}
