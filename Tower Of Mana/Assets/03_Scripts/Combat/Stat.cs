﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]
    private float baseValue = 0;

    public float GetValue()                       //allows us to use this method and apply any modifiers before returning the value!
    {
        return baseValue;
    }
}
