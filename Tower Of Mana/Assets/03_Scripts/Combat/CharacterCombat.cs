﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]                  //makes sure CharacterStats is attached
public class CharacterCombat : MonoBehaviour
{
    CharacterStats myStats;

    private void Start()
    {
        myStats = GetComponent<CharacterStats>();           //reference for current character's stats
    }

   
    public void Attack(CharacterStats targetStats)                  //targetStats references to target hit
    {
        {
            targetStats.TakeDamage(myStats.damage.GetValue());      //deals damage to target based on current character's own damage
        }
    }
}
