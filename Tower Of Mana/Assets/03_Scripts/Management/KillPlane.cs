﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour
{
    CanvasItems canvasItems;

    private void Start()
    {
        canvasItems = FindObjectOfType<CanvasItems>();
    }
    private void OnTriggerEnter(Collider other)
    {
        canvasItems.loseScreen.SetActive(true);
        Time.timeScale = 0f;
    }
}
