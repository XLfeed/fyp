﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("There is already a GameManager in scene =n=???");
        }
        instance = this;
    }

    public RoomsGenerator RoomGen;
    public GameObject PlayerPrefab;
    public GameObject BossPrefab;
    public Camera Camera;

    public GameObject Player;
    public float playerOffset;

    public NavMeshSurface[] newNavMesh;

    [SerializeField] public int enemyCount;
    private BaseRoom currentRoom;
    private FogGenerator fogGenerator;

    public GameObject BossRoomLayout;
    public GameObject ShopRoomLayout;
    public GameObject[] StartRoomLayout;
    public GameObject[] roomLayout;
    public GameObject[] enemyTypes;
    public GameObject[] propTypes;
    public GameObject[] crateTypes;
    public GameObject[] crateDrops;

    public Animator Levelloader;

    public Text healthPotionsText;
    public Text coinsText;

    private void Start()
    {
        fogGenerator = FindObjectOfType<FogGenerator>();
    }

    private void Update()
    {
        if(enemyCount == 0)
        {
            enemyCount = -1;
            if(currentRoom != null)
            {
                currentRoom.RoomCleared();
            }
            currentRoom = null;
        }
    }

    public void SpawnPlayer(GameObject StartRoom)
    {
        Player = Instantiate(PlayerPrefab, StartRoom.transform.position + new Vector3(0, playerOffset, 0), Quaternion.identity);
        Levelloader.SetTrigger("PlayerSpawned");
    }

    public void SpawnBoss(GameObject EndRoom)
    {
        GameObject Boss = Instantiate(BossPrefab, EndRoom.transform.position, Quaternion.identity);
    }

    public void EnterRoom(GameObject _tpPoint, BaseRoom _baseRoom)
    {
        Player.SetActive(false);
        Player.transform.position = _tpPoint.transform.position;
        Player.SetActive(true);

        currentRoom = _baseRoom;
        enemyCount = _baseRoom.spawnProp.roomtype.enemy.Length;

    }

    public void UpdateEnemyCount(int diff)
    {
        enemyCount += diff;
    }

    public int GetEnemyCount()
    {
        return enemyCount;
    }
}
