﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProp : MonoBehaviour
{
    public Layout roomtype;

    public void PopulateRoom()
    {
        int randNum = Random.Range(0, GameManager.instance.roomLayout.Length);
        GameObject layout = Instantiate(GameManager.instance.roomLayout[randNum], this.transform.position, Quaternion.identity);
        roomtype = layout.GetComponent<Layout>();
        roomtype.SpawnContent(this);
    }
    public void PopulateBossRoom()
    {
        GameObject layout = Instantiate(GameManager.instance.BossRoomLayout, this.transform.position, Quaternion.identity);
        roomtype = layout.GetComponent<Layout>();
        roomtype.SpawnBoss(this);
    }
    public void PopulateStartRoom()
    {
        int randNum = Random.Range(0, GameManager.instance.StartRoomLayout.Length);
        GameObject layout = Instantiate(GameManager.instance.StartRoomLayout[randNum], this.transform.position, Quaternion.identity);
        roomtype = layout.GetComponent<Layout>();
        roomtype.SpawnStart(this);
    }
    public void PopulateShopRoom()
    {
        //spawn shopkeeper
        GameObject layout = Instantiate(GameManager.instance.ShopRoomLayout, this.transform.position, Quaternion.identity);
        roomtype = layout.GetComponent<Layout>();
        roomtype.SpawnShop(this);
    }
}
