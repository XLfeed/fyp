﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StairExit : MonoBehaviour
{
    int nextSceneLoad;
    private GameManager gameMaster;

    private void Awake()
    {
        gameMaster = FindObjectOfType<GameManager>();
        nextSceneLoad = SceneManager.GetActiveScene().buildIndex + 1; // loads next level
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))   // if player touch stairs (cube for now)
        {
            gameMaster.Levelloader.SetTrigger("Start");
            StartCoroutine(LoadLevel());
           
          //  SceneManager.LoadScene(nextSceneLoad);
        }
    }

    IEnumerator LoadLevel()
    {
        yield return new WaitForSeconds(1f);
       // gameMaster.Levelloader.SetTrigger("Start");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(nextSceneLoad);
    }
}
