﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeLevelExit : MonoBehaviour
{
    [Header("Spawn")]
    [SerializeField] GameObject stairsPrefab;               // Slime Boss Exit to next level
    //[SerializeField] GameObject stairSpawn;
    [SerializeField] GameObject spawnPoint3;
    BaseRoom br;
    GameManager gameMaster;

    // Start is called before the first frame update
    void Start()
    {
        br = GetComponent<BaseRoom>();
        gameMaster = FindObjectOfType<GameManager>();       
    }

    void OnEnable()
    {
        //GameManager.instance.UpdateEnemyCount(4);
    }

    // Update is called once per frame
    void Update()
    {
        SpawnOnDeath();
    }

    void SpawnOnDeath()
    {
        if (gameMaster.enemyCount <= 0)                  // if no more enemies
        {
            
            
            Instantiate(stairsPrefab, spawnPoint3.transform.position, Quaternion.identity); // spawn exit(cube for now)
            Destroy(gameObject);                                                                                       // destroy spawner           
        }
    }
}
