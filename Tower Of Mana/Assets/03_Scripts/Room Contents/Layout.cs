﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layout : MonoBehaviour
{
    public GameObject[] enemy;
    public GameObject[] props;
    public GameObject[] crates;

    private List<GameObject> enemies;

    private SpawnProp contentSpawner;

    GameManager gameMaster;
    public GameObject stairPrefab;

    void Awake()
    {
        enemies = new List<GameObject>();
    }

    void Update()
    {
        SpawnOnDeath();
    }

    void Start()
    {
        
        gameMaster = FindObjectOfType<GameManager>();
    }

    //room layout and spawning items
    public void SpawnContent(SpawnProp spawnProp)
    {
        contentSpawner = spawnProp;
        //for spawning enemies and disabling them
        for (int i = 0; i < enemy.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.enemyTypes.Length);
            GameObject EnemySpawned = Instantiate(GameManager.instance.enemyTypes[randNum], enemy[i].transform.position, Quaternion.identity);
            enemies.Add(EnemySpawned);
            EnemySpawned.SetActive(false);
        }

        for (int i = 0; i < props.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.propTypes.Length - 1);
            Instantiate(GameManager.instance.propTypes[randNum], props[i].transform.position, Quaternion.identity);
        }

        for (int i = 0; i < crates.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.crateTypes.Length);
            Instantiate(GameManager.instance.crateTypes[randNum], crates[i].transform.position, Quaternion.identity);
        }
    }

    public void EnableEnemy()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].SetActive(true);
        }
    }

    public void SpawnShop(SpawnProp spawnProp)
    {
        contentSpawner = spawnProp;
        for (int i = 0; i < props.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.propTypes.Length - 1);
            Instantiate(GameManager.instance.propTypes[randNum], props[i].transform.position, Quaternion.identity);
        }
        for (int i = 0; i < crates.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.crateTypes.Length - 1);
            Instantiate(GameManager.instance.crateTypes[randNum], crates[i].transform.position, Quaternion.identity);
        }
    }

    public void SpawnBoss(SpawnProp spawnProp)
    {
        contentSpawner = spawnProp;
        for (int i = 0; i < enemy.Length; i++)
        {
            GameObject EnemySpawned = Instantiate(GameManager.instance.BossPrefab, enemy[i].transform.position, Quaternion.identity);
            enemies.Add(EnemySpawned);
            DisableBoss();
        }

        for (int i = 0; i < props.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.propTypes.Length - 1);
            Instantiate(GameManager.instance.propTypes[randNum], props[i].transform.position, Quaternion.identity);
        }
        for (int i = 0; i < crates.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.crateTypes.Length - 1);
            Instantiate(GameManager.instance.crateTypes[randNum], crates[i].transform.position, Quaternion.identity);
        }
    }
    public void SpawnStart(SpawnProp spawnProp)
    {
        contentSpawner = spawnProp;
        for (int i = 0; i < props.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.propTypes.Length - 1);
            Instantiate(GameManager.instance.propTypes[randNum], props[i].transform.position, Quaternion.identity);
        }
        for (int i = 0; i < crates.Length; i++)
        {
            int randNum = Random.Range(0, GameManager.instance.crateTypes.Length - 1);
            Instantiate(GameManager.instance.crateTypes[randNum], crates[i].transform.position, Quaternion.identity);
        }
    }

    void DisableBoss()
    {
        //disable boss scripts
        enemies[0].SetActive(false);
    }

    void SpawnOnDeath()
    {
        if (gameMaster.enemyCount <= 0)                  // if no more enemies
        {
            if (stairPrefab != null)
            { 
                stairPrefab.SetActive(true);
            }
        }
    }
}
