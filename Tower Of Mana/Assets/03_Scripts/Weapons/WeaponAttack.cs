﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttack : MonoBehaviour
{
    //Script for weapon hitbox to deal damage to enemies
    Collider hitbox;

    private void Start()
    {
        hitbox = GetComponent<Collider>();
    }

    private void Update()
    {
        if (PlayerController.isAttacking == false)                                             //check if player is currently attacking
        {
            hitbox.enabled = false;                                                            //disables hitbox
        }
        else
        {
            hitbox.enabled = true;                                                             //enables hitbox
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))                                           //if i hit an enemy
        {
            EnemyController hit = other.gameObject.GetComponent<EnemyController>();         //gets reference to enemy hit
            if (hit != null)                                                                //prevents errors
            {
                hit.TakeDamage();
            }
            SlimeBossController hit1 = other.gameObject.GetComponent <SlimeBossController>();         //gets reference to enemy hit
            if (hit1 != null)                                                                          //prevents errors
            {
                Debug.Log("Hittt");
                hit1.TakeDamage();
            }
            SkeletonController hit2 = other.gameObject.GetComponent<SkeletonController>();         //gets reference to enemy hit
            if (hit2 != null)                                                                          //prevents errors
            {
                hit2.TakeDamage();
            }
            GolemController hit3 = other.gameObject.GetComponent<GolemController>();         //gets reference to enemy hit
            if (hit3 != null)                                                                          //prevents errors
            {
                hit3.TakeDamage();
            }
            TreantController hit4 = other.gameObject.GetComponent<TreantController>();         //gets reference to enemy hit
            if (hit4 != null)                                                                          //prevents errors
            {
                hit4.TakeDamage();
            }
        }

        if (other.gameObject.CompareTag("Crate"))
        {
            Crate crate = other.gameObject.GetComponent<Crate>();
            crate.Explode();
        }
    }
}
