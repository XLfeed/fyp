﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]

public class PlayerController : MonoBehaviour
{
    public static bool isAttacking = false;                             // weapon script

    [Header("References")]
    CharacterController controller;
    PlayerStats mystats;
    PlayerAnimatorController animCon;
    Animator anim;                                                      //animator Conditions: 0 = idle, 1 = run, 2 = attack, 3 = dash

    [Header("Movement")]
    public float runSpeed = 4.0f;
    float currentSpeed;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    public float gravity = 20.0f;
    bool isSwinging;

    Vector3 moveDirection;
    Vector3 zeroDir = Vector3.zero;

    [Header("Attack Rotation")]
    Camera mainCam;
    public LayerMask floorMask;                                            // get ray hit on FLOOR LAYER only

    [Header("Potions")]
    public int healthPotionCount = 0;
    public float healthIncrease = 20f;
    [Range(0f, 99f), Tooltip("Percentage Slowed")]
    public float slowFactor = 15;
    bool isDrinking = false;

    [Header("Coins")]
    public int coinCount = 0;

    [Header("View Obstruct")]
    public LayerMask obstacleMask;
    public Vector3 offset;

    [Header("Sound Stuffy")]
    GameObject audioManager;
    AudioSource swingAudio;
    public AudioClip swingSFX;
    // AudioSource drinkAudio;
    // public AudioClip drinkSFX;

    public PlayerMaterials[] playerMaterials;
    [System.Serializable]
    public struct PlayerMaterials
    {
        public string name;
        public Material material;
    }

    public SkinnedMeshRenderer Armor;
    public SkinnedMeshRenderer Body;
    public SkinnedMeshRenderer Clothes;
    public SkinnedMeshRenderer Hair;

    Transform obst;
    MeshRenderer[] meshRenderers;
    Transform currentObj;
    

    float x;
    float z;

   
    private void Start()
    {
        controller = GetComponent<CharacterController>();
        mystats = GetComponent<PlayerStats>();
        anim = GetComponent<Animator>();
        animCon = GetComponent<PlayerAnimatorController>();
        mainCam = GameManager.instance.Camera;
        currentSpeed = runSpeed;

        #region PlayerPrefs
        healthPotionCount = PlayerPrefs.GetInt("HealthPotionCount");
        coinCount = PlayerPrefs.GetInt("CoinCount");
        #endregion

        GameManager.instance.healthPotionsText.text = "❤️❤️: " + healthPotionCount;
        GameManager.instance.coinsText.text = "$$: " + coinCount;

        audioManager = GameObject.FindGameObjectWithTag("Audio");
        swingAudio = audioManager.transform.GetChild(10).GetComponent<AudioSource>();
        // drinkAudio = audioManager.transform.GetChild(11).GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        if (animCon!= null)
        {
            animCon.canAttack = true;
            animCon.canMove = true;
            animCon.canRoll = true;
            animCon.canSwap = true;
            isDrinking = false;
            currentSpeed = runSpeed;
        }
    }

    private void Update()
    {
        
        PlayerMove();       // w a s d
        ViewObstruct();     // wall render deactivate
        PlayerAttack();     // mouseclick(0)
        PlayerDash();       // space
        eInput();           // e: sheathe weapon OR e: consume potion
        WeaponSwap();       // q

        //PlayerPrefs.SetInt("CoinCount", coinCount);
        //PlayerPrefs.SetInt("HealthPotionCount", healthPotionCount);
    }

    #region AnimationEvents
    public void RecoverSpeed()
    {
        isDrinking = false;
        currentSpeed = runSpeed;
    }

    public void StartAttacking()
    {
        isAttacking = true;
    }

    public void StopAttacking()
    {
        isAttacking = false;
    }
    #endregion

    void PlayerMove()
    {
        

        if (!animCon.canMove)
        {
            return;
        }

        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(x, 0f, z).normalized;

        if (moveDirection.magnitude >= 0.1f)       //on input pressed
        {
            isSwinging = true;
            //anim.SetInteger("Condition", 1);                                //starts run animation
            float targetAngle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;                                          //calculates the angle to turn
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);          //finds smooth angle for turning
            transform.rotation = Quaternion.Euler(0f, angle, 0f);                                                                       //turns

            controller.Move(moveDirection * currentSpeed * Time.deltaTime);                                                                //move player
            anim.SetFloat("Speed", currentSpeed/runSpeed);
        }
        else if (x == 0 && z == 0)
        {
            anim.SetFloat("Speed", 0);

            //anim.SetInteger("Condition", 0);                                                                                            //starts idle animation
        }

        zeroDir.y -= gravity * Time.deltaTime;                        //Applies gravity to character controller
        controller.Move(zeroDir * Time.deltaTime);                    //Applied twice due to gravity being acceleration

        if (isSwinging)
        {
            swingAudio.Play();
        }
        else
        {
            swingAudio.Stop();
        }
    }

    
    void ViewObstruct()
    {
        RaycastHit hit;

        Debug.DrawRay(transform.position + offset, (mainCam.transform.position - transform.position), Color.red);                               // debug ray in Unity Engine
        if (Physics.Raycast(transform.position + offset, mainCam.transform.position - transform.position, out hit, 1000f, obstacleMask))        // draws ray from player to camera, only hits objects with obstacleMask Layer
        {
            obst = hit.transform;               
            
            if (hit.transform != currentObj)            // if obstacle hit is not obstacle currently hit
            {                              
                if (meshRenderers != null)                  
                { 
                    for (int i = 0; i < meshRenderers.Length; i++)              // for each mesh renderer in children of obstacle
                    {
                        if (meshRenderers[i] != null)             
                        { 
                            meshRenderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;        // reactivates obstacle per mesh renderer in child
                            meshRenderers[i] = null;                                                                // removes mesh renderer from array
                        }
                    }              
                }

                meshRenderers = hit.transform.GetComponentsInChildren<MeshRenderer>();                              
                foreach (MeshRenderer meshrenderer in meshRenderers)
                {                   
                    meshrenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;            // deactivates each child renderer using Unity Engine rendering
                }
                currentObj = obst;             
            }

        }

        else
        {
            currentObj = null;
            if (meshRenderers != null)
            { 
                for (int i = 0; i < meshRenderers.Length; i++)
                {
                    if (meshRenderers[i] != null)
                    {
                        meshRenderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;        // reactivates obstacle per mesh renderer in child
                        meshRenderers[i] = null;                                                                // removes mesh renderer from array
                    }
                }
            }
        }
    }
    


    /*void ViewObstruct()
    {
        RaycastHit hit;

        Debug.DrawRay(transform.position + offset, (mainCam.transform.position - transform.position), Color.red);                               // debug ray in Unity Engine
        if (Physics.Raycast(transform.position + offset, mainCam.transform.position - transform.position, out hit, 1000f, obstacleMask))        // draws ray from player to camera, only hits objects with obstacleMask Layer
        {
            if (playerMaterials.Length != 0)
            {
                Armor.material = playerMaterials[0].material;
                Body.material = playerMaterials[5].material;
                Clothes.material = playerMaterials[6].material;
                Hair.material = playerMaterials[7].material;
            }
        }
        else
        {
            if (playerMaterials.Length != 0)
            {
                Armor.material = playerMaterials[0].material;
                Body.material = playerMaterials[1].material;
                Clothes.material = playerMaterials[2].material;
                Hair.material = playerMaterials[3].material;
            }
        }
    }
    */


    void eInput()
    {
        if (Input.GetKey(KeyCode.E))
        {
            if (anim.GetInteger("Weapon") > 0 && animCon.canSwap)
            {
                animCon.CallSheathe();                  // player animator controller coroutine
            }
            else if (anim.GetInteger("Weapon") == 0 && animCon.canAttack && healthPotionCount > 0 && mystats.currentHealth != mystats.maxHealth)
            {
                healthPotionCount -= 1;
                PlayerPrefs.SetInt("HealthPotionCount", healthPotionCount);
                GameManager.instance.healthPotionsText.text = "❤️❤️: " + healthPotionCount;
                isDrinking = true;

                anim.SetTrigger("Drink");
                
            }
        }

        if (isDrinking)
        {
            
            currentSpeed = runSpeed * (100-slowFactor)/100;
            mystats.HealthPotion(healthIncrease);
            isDrinking = false;
            
        }
        
    }


    void PlayerAttack()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);                
        RaycastHit hit;

        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.white);
       
        if (!animCon.canAttack)
        {
            return;
        }

        if ( Input.GetMouseButtonDown(0) && animCon.canAttack)
        {

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, floorMask))          //ray hits Floor layer
            {
                Vector3 targetDir = new Vector3(hit.point.x, transform.position.y, hit.point.z);    //get position whr ray hits

                Quaternion attackDir = Quaternion.LookRotation(targetDir - transform.position);     //get rotation of player to point clicked
                
                transform.rotation = Quaternion.Lerp(transform.rotation, attackDir, Time.deltaTime * 100f);     //rotates player

                animCon.Attack();       //Attack animation
            }

        }
    }


    void PlayerDash()
    {
        if (!animCon.canRoll)
        { 
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space) && animCon.canRoll)
        {
            animCon.Roll();
        }
    }

    void WeaponSwap()
    {
        if (!animCon.canSwap)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Q) && animCon.canSwap)
        {
            animCon.WeaponSwap();
        }
    }

}




