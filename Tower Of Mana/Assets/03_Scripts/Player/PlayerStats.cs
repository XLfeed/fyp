﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : CharacterStats
{

    private float lerpHealth;                                       //lerp value for smooth healthbar
    public float decreaseRate = 0.1f;

    Animator anim;

    private CanvasItems UIcanvas;

    public override void Start()
    {   if (PlayerPrefs.GetFloat("CurrentHealth") == 0)             // if player health == 0 (happens when player starts new game)
        {
            base.Start();                                           // set current health to max health
            PlayerPrefs.SetFloat("CurrentHealth", maxHealth);       // set player health to max health
        }
        else 
        {
            currentHealth = PlayerPrefs.GetFloat("CurrentHealth");
        }
        UIcanvas = FindObjectOfType<CanvasItems>();
        anim = GetComponent<Animator>();                   //Animator reference
        lerpHealth = maxHealth;                                      //sets healthbar fill value to max on start
    }

    private void Update()
    {
        //if current health changes, smoothly change healthbar fill
        if (lerpHealth != currentHealth)
        {
            StartCoroutine(LerpHealth());                             
        }
    }

    public override void Die()
    {
        base.Die();
        Time.timeScale = 0f;                   
        UIcanvas.loseScreen.SetActive(true);                         //Sets lose screen active
        //anim.SetBool(Die, true);                                   //Plays Death Animation
    }

    IEnumerator LerpHealth()                                         //coroutine for smooth healthbar
    {
        yield return new WaitForEndOfFrame();

        float HealthSmoothing = Mathf.Lerp(lerpHealth, currentHealth, decreaseRate);
        lerpHealth = HealthSmoothing;
        UIcanvas.Health.fillAmount = lerpHealth / maxHealth;
    }

}
