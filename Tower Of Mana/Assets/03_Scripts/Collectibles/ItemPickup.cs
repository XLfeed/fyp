﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickup : MonoBehaviour
{
    public enum ItemName { Coin, HealthPotion, Item3 };
    public ItemName itemName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController pc = other.transform.gameObject.GetComponent<PlayerController>();
            if (pc != null)
            {
                if (itemName == ItemName.Coin)
                {
                    //play sfx
                    pc.coinCount += 1;
                    PlayerPrefs.SetInt("CoinCount", pc.coinCount);
                    GameManager.instance.coinsText.text = "$$: " + pc.coinCount;
                    Destroy(gameObject);
                }

                if (itemName == ItemName.HealthPotion)
                {
                    pc.healthPotionCount += 1;
                    PlayerPrefs.SetInt("HealthPotionCount", pc.healthPotionCount);
                    GameManager.instance.healthPotionsText.text = "❤️❤️: " + pc.healthPotionCount;
                    Destroy(gameObject);
                }
            }
        }
    }
}
