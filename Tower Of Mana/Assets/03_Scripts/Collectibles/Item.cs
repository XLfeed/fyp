﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public enum ItemName { Item1, HealthPotion, Item3 };
    public ItemName itemName;

    public string description;
    public int cost;
    public GameObject textActive;
    public Text itemText;
    public Text costText;

    protected virtual void Awake()
    {
        
    }

    protected virtual void Update()
    {
        itemText.text = description;
        costText.text = "$" + cost;
    }

    public IEnumerator Pulse()
    {
        ItemSelect.canPulse = false;

        for (float i = 0f; i <= 1f; i += .1f)
        {
            transform.localScale = new Vector3(
                (Mathf.Lerp(transform.localScale.x, transform.localScale.x + 0.02f, Mathf.SmoothStep(0f, 1f, i))),
                (Mathf.Lerp(transform.localScale.y, transform.localScale.y + 0.02f, Mathf.SmoothStep(0f, 1f, i))),
                (Mathf.Lerp(transform.localScale.z, transform.localScale.z + 0.02f, Mathf.SmoothStep(0f, 1f, i)))
                );

            yield return new WaitForSeconds(0.03f);

        }

        for (float i = 0f; i <= 1f; i += .1f)
        {
            transform.localScale = new Vector3(
                (Mathf.Lerp(transform.localScale.x, transform.localScale.x - 0.02f, Mathf.SmoothStep(0f, 1f, i))),
                (Mathf.Lerp(transform.localScale.y, transform.localScale.y - 0.02f, Mathf.SmoothStep(0f, 1f, i))),
                (Mathf.Lerp(transform.localScale.z, transform.localScale.z - 0.02f, Mathf.SmoothStep(0f, 1f, i)))
                );

            yield return new WaitForSeconds(0.03f);
        }

        ItemSelect.canPulse = true;

    }
}
