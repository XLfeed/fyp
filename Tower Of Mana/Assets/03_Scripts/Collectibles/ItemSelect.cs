﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSelect : MonoBehaviour
{
    public LayerMask itemMask;                                                  // ray only hits shop items layer
    Item itemSelected;

    public static bool canPulse;

    private void Start()
    {
        canPulse = true;
    }

    private void Update()
    {
        SelectItem();
    }

    void SelectItem()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);            // draws ray from camera to mouse pos on screen
        RaycastHit hit; 

        if(Physics.Raycast(ray, out hit, 1000, itemMask))                       
        {
            Item item = hit.transform.gameObject.GetComponent<Item>();          // get reference to item selected
            PlayerController pc = GameManager.instance.Player.GetComponent<PlayerController>();

            if (itemSelected != item && itemSelected != null)                   // if mouse over different item or no item
            {
                itemSelected.textActive.SetActive(false);                       // deactivates description text 
            }

            item.textActive.SetActive(true);                                    // activates description text
            itemSelected = item;
            
            if (canPulse && pc.coinCount >= item.cost)
            { 
                item.StartCoroutine("Pulse");                                       // starts pulsing coroutine
            }

            if (Input.GetMouseButtonDown(0))
            {
                if(pc.coinCount >= item.cost)
                { 
                    MeshRenderer mr = hit.transform.gameObject.GetComponent<MeshRenderer>();
                    Collider col = hit.transform.gameObject.GetComponent<Collider>();
                    mr.enabled = false;
                    col.enabled = false;
                    //play sfx
                    pc.coinCount -= item.cost;
                    PlayerPrefs.SetInt("CoinCount", pc.coinCount);
                    GameManager.instance.coinsText.text = "$$: " + pc.coinCount;
                }
            }
        }
        else
        {
            if (itemSelected != null)                                           
            { 
                itemSelected.textActive.SetActive(false);
            }
        }
    }
}
