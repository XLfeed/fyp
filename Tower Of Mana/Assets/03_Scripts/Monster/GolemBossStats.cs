﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GolemBossStats : CharacterStats
{
    private CanvasItems UIcanvas;
    Animator anim;

    public int nextSceneLoad;

    [Header("Spawn")]
    [SerializeField] GameObject stairsPrefab;               // Golem Boss Exit to next level
    [SerializeField] GameObject stairSpawn;
    [SerializeField] GameObject spawnPoint4;

    public override void Start()
    {
        base.Start();
        nextSceneLoad = SceneManager.GetActiveScene().buildIndex + 1;
        UIcanvas = FindObjectOfType<CanvasItems>();
        anim = GetComponentInChildren<Animator>();
    }

    public override void Die()
    {
        base.Die();
        stairSpawn = Instantiate(stairsPrefab, spawnPoint4.transform.position, Quaternion.identity) as GameObject;  // Spawn stair prefab  after boss death
        //anim.SetBool("Die", true);                            //create a parameter for Die boolean to play death animation
        Destroy(this.gameObject);
        // SceneManager.LoadScene(nextSceneLoad);
        // UIcanvas.winScreen.SetActive(true);                  //TEMPORARY win condition: boss dies
        // Time.timeScale = 0.0f;

    }
}
