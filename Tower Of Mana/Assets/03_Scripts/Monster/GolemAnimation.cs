﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class GolemAnimation : MonoBehaviour
{
    BoxCollider bc;

    [Header("Sound Stuffy")]
    GameObject audioManager;
    AudioSource golemAttackAudio;
    public AudioClip golemAttackSFX;

    void Start()
    {       
        bc = gameObject.GetComponentInChildren<BoxCollider>();
        audioManager = GameObject.FindGameObjectWithTag("Audio");
        golemAttackAudio = audioManager.transform.GetChild(8).GetComponent<AudioSource>();
    }
    void DealDamage()
    {
        bc.enabled = true;
        golemAttackAudio.PlayOneShot(golemAttackSFX);
        //combat.Attack(playerStats);                                                                // Enemy deals damage to player    
    }

    void StopDamage()
    {
        bc.enabled = false;
    }
}
