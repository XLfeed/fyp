﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossBow : MonoBehaviour
{
    public GameObject arrowPrefab;
    public float speed = 100f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Arrow()
    {
        GameObject arrowSpawn = Instantiate(arrowPrefab, transform.position, Quaternion.identity) as GameObject;
        Rigidbody rb = arrowSpawn.GetComponent<Rigidbody>();
        rb.AddForce(Vector3.forward * speed);
    }
}
