﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPlayer : MonoBehaviour
{
    // "#region (name of region)" is used like a folder for codes for easier navigation
    // Press the + sign on the left to expand
    #region Singleton 
    // The singleton pattern is a design pattern that enforces the existence of only ONE object pf a specific type at any given moment.
    // A singleton is an actual object that can be referenced and passed around and this case being used for, our player.
    // Line line intersection: http://wiki.unity3d.com/index.php/Singleton

    public static FindPlayer instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    // Tooltip is like a label/comment, you can see it in unity by mouse over/hovering in the inspector on whatever that you've added tooltip
    // In this case, in GameManager under FindPlayer script, hover over "Player" will show "Add player ref here"
    [Tooltip("Add player ref here")]
    public GameObject player;
}

    