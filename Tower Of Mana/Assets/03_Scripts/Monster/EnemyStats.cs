﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
    Animator anim;
    bool isUpdated = false;

    public override void Start()
    {
        base.Start();                                 // base function
        anim = GetComponentInChildren<Animator>();    // overriding
    }

    public override void Die()
    {
        base.Die();
        //anim.SetBool("Die", true);                //create a parameter for Die boolean to play death animation
        if (!isUpdated)
        {
            GameManager.instance.UpdateEnemyCount(-1);
            isUpdated = true;
        }
        Destroy(gameObject);
    }
}
