﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SlimeBossStats : CharacterStats
{
    private CanvasItems UIcanvas;
    Animator anim;

    //int nextSceneLoad;

    [Header("Spawn")]
    [SerializeField] GameObject fourSlimePrefab;               // Prefab of 4 slimes
    [SerializeField] GameObject slimeSpawn;
    [SerializeField] GameObject spawnPoint2;

    public override void Start()
    {
        base.Start();
        //nextSceneLoad = SceneManager.GetActiveScene().buildIndex + 1;
        UIcanvas = FindObjectOfType<CanvasItems>();
        anim = GetComponentInChildren<Animator>();    
    }

    public override void Die()
    {
        base.Die();
        slimeSpawn = Instantiate(fourSlimePrefab, spawnPoint2.transform.position, Quaternion.identity) as GameObject;  // Spawn prefab of 4 slimes after boss death
        //anim.SetBool("Die", true);                            //create a parameter for Die boolean to play death animation
        GameManager.instance.UpdateEnemyCount(3);
        Destroy(gameObject);
        // SceneManager.LoadScene(nextSceneLoad);
        // UIcanvas.winScreen.SetActive(true);                  //TEMPORARY win condition: boss dies
        // Time.timeScale = 0.0f;

    }
}
