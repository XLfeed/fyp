﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnimation : MonoBehaviour
{
    [Header("Sound Stuffy")]
    GameObject audioManager;
    AudioSource skeletonAttackAudio;
    public AudioClip skeletonAttackSFX;

    [Header("Referencing")]
    public CrossBow cb;

    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("Audio");
        skeletonAttackAudio = audioManager.transform.GetChild(6).GetComponent<AudioSource>();
        cb = GetComponent<CrossBow>();
    }

    // Update is called once per frame
    void Shoot()
    {
        skeletonAttackAudio.PlayOneShot(skeletonAttackSFX);                                  // Play skeleton attck sound
        cb.Arrow();
    }
}
