﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RockShower : MonoBehaviour
{
    GameObject player;
    CharacterCombat combat;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.Player;
        combat = GetComponent<CharacterCombat>();                                // Reference to combat (attack)
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CharacterStats playerStats = player.GetComponent<CharacterStats>();                            // Gets reference to player's stats
            if (playerStats != null)
            {
                combat.Attack(playerStats);                                                                // Enemy deals damage to player
            }
            Destroy(gameObject);                                                                           // Destroy rock
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
