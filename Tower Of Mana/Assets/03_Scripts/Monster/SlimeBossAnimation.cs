﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class SlimeBossAnimation : MonoBehaviour
{
    GameObject player;
    CharacterCombat combat;
    CapsuleCollider cc;
    PlayerStats pc;

    void Start()
    {
        player = GameManager.instance.Player;
        pc = player.GetComponent<PlayerStats>();
        combat = GetComponent<CharacterCombat>();                                // Reference to combat (attack)
        cc = gameObject.GetComponentInParent<CapsuleCollider>();
    }
    void DealDamage()
    {
        cc.enabled = true;
        combat.Attack(pc);                                                                // Enemy deals damage to player    
    }

    void StopDamage()
    {
        cc.enabled = false;
    }
}
