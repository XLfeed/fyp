﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeController : EnemyController
{
    [Header("Sound Stuffy")]   
    AudioSource slimeAttackAudio;
    public AudioClip slimeAttackSFX;
    AudioSource takeDamageAttackAudio;
    public AudioClip takeDamageAttackSFX;

    public override void Start()
    {
        base.Start();
        
        slimeAttackAudio = audioManager.transform.GetChild(4).GetComponent<AudioSource>();
        takeDamageAttackAudio = audioManager.transform.GetChild(9).GetComponent<AudioSource>();
    }

    public override void PopUpDamage()
    {
        base.PopUpDamage();
        takeDamageAttackAudio.PlayOneShot(takeDamageAttackSFX);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CollisionAttack();
        }
    }

    public override void CollisionAttack()
    {
        base.CollisionAttack();
        slimeAttackAudio.PlayOneShot(slimeAttackSFX);                                                  // Play slime attck sound
    }
}
