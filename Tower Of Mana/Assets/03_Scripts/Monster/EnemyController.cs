﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class EnemyController : MonoBehaviour
{
    [Header("Follow Character")]
    [Tooltip("Check if player is within CHASE range")]
    public float viewRadius = 8f;                                                // Enemy CHASE detection radius
    bool isChasing = false;                                                      // Bool to check if enemy is chasing/ patrolling

    [Header("Attack Character")]
    [Tooltip("Check if player is within ATTACK range")]
    public float attackRadius = 1.5f;                                            // Enemy ATTACK detection radius

    public GameObject player;
    Transform playerPos;
    NavMeshAgent agent;

    [Header("Script References")]
    CharacterStats myStats;
    public CharacterCombat combat;

    [Header("UI:Health")]
    public Image healthBar;
    [SerializeField] float updateSpeedSeconds = 0.5f;
    public GameObject popUpDamagePrefab;

    [Header("Animation")]
    private Animator anim;

    [Header("Sound Stuffy")]
    public GameObject audioManager;
    
    //EnemyStats eneStats;

    private void Awake()
    {
        GetComponentInParent<CharacterStats>().OnHealthPercentChanged += HealthChanged; // Whenever "OnHealthPercentChanged", call"HealthChanged"
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        player = GameManager.instance.Player;                                              // Reference to player
        playerPos = player.transform;                                            // Reference to player location
        myStats = GetComponent<CharacterStats>();                                // Reference to stats
        combat = GetComponent<CharacterCombat>();                                // Reference to combat (attack)
        //eneStats = GetComponent<EnemyStats>();                                   // Reference to Enemy's stats

        agent = GetComponent<NavMeshAgent>();                                    // Reference to NavMesh Agent

        anim = GetComponentInChildren<Animator>();
        audioManager = GameObject.FindGameObjectWithTag("Audio");
        

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Chase();
    }

    void FaceTarget()
    {
        Vector3 direction = (playerPos.position - transform.position).normalized;                      // Get player's direction
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);  // Quaternion.Slerp is to add sotthing to rotation
    }

    public virtual void CollisionAttack()
    {
        CharacterStats playerStats = player.GetComponent<CharacterStats>();                            // Gets reference to player's stats
        if (playerStats != null)
        {
            combat.Attack(playerStats);                                                                // Enemy deals damage to player           
        }
    }

    // Enemy ATTACK!!
    public virtual void AttackTarget()
    {
        anim.SetBool("Attack", true);                                                                  // Play Attack Animation
    }

    // Enemy Takes Damage from player
    public void TakeDamage()                                                                           // Called from player's weapon trigger
    {
        CharacterCombat playerCombat = player.GetComponent<CharacterCombat>();                         // Gets reference to player's damage  
        if (playerCombat != null)
        {
            playerCombat.Attack(myStats);                                                              // Player deals damage to enemy
            healthBar.fillAmount = myStats.currentHealth / myStats.maxHealth;
            PopUpDamage();                                                                             // Spawn damage
        }
    }

    // CHASEEE!!
    void Chase()
    {
        if (player != null)
        {
            float distance = Vector3.Distance(playerPos.position, transform.position);  // Get distance between enemy and player

            if (distance <= viewRadius)                                                 // Check if player is within range, if yes, CHASEEEE!
            {
                isChasing = true;
                agent.SetDestination(playerPos.position);                               // Go to player

                if (distance <= attackRadius)                                           // I'm right next to you... enemy is within range to attack
                {
                    FaceTarget();
                    AttackTarget();
                }
                else
                {
                    anim.SetBool("Attack", false);
                }

            }
            else
            {
                isChasing = false;
            }
        }
    }

    private void HealthChanged(float percent)                                           // To call start on a Coroutine
    {
        StartCoroutine(ChangeToPercent(percent));
    }

    private IEnumerator ChangeToPercent(float percent)                                  // Coroutine is to add lerp in hp reduction or otherwise by changing hp into a percentage
    {
        float beforeChangePercent = healthBar.fillAmount;
        float elapsed = 0f;                                                             // Amount of time passed

        while (elapsed < updateSpeedSeconds)                                            // Make sure time passed is lesser than update speed to run the while loop
        {
            elapsed += Time.deltaTime;
            healthBar.fillAmount = Mathf.Lerp(beforeChangePercent, percent, elapsed / updateSpeedSeconds);  // Update fill amount
            yield return null;
        }

        healthBar.fillAmount = percent;                                                 // Set at exact amount after lerp
    }

    // Instantiate damage numbers
    public virtual void PopUpDamage()
    {
        GameObject floatingNumbers = Instantiate(popUpDamagePrefab, transform.position, Quaternion.identity) as GameObject;
        floatingNumbers.transform.GetChild(0).GetComponent<TextMesh>().text = "35";    // Text to be damage dealt by player
        
    }

    // The radius where enemy detects player
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewRadius);                                         // Gizmos to be shown at enemy's position
    }
}
