﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class GolemBossController : MonoBehaviour
{
    [Header("See Player")]
    [Tooltip("Check if player is within range")]
    public float viewRadius = 7f;                                                // Enemy detection radius

    [Header("Attack Character")]
    [Tooltip("Check if player is within ATTACK range")]
    public float attackRadius = 7f;                                            // Enemy ATTACK detection radius

    [Header("Player Stuff")]
    GameObject player;
    Transform playerPos;
    NavMeshAgent agent;

    [Header("Rock Spawning")]
    [SerializeField] GameObject spawnPoint5, spawnPoint6, spawnPoint7, spawnPoint8, spawnPoint9;
    [SerializeField] GameObject rockPrefab;
    [SerializeField] GameObject rockSpawn;
    [SerializeField] float spawnTime = 5f;

    [Header("Script References")]
    CharacterStats myStats;
    CharacterCombat combat;
    EnemyStats eneStats;

    [Header("UI:Health")]
    public Image healthBar;
    [SerializeField] float updateSpeedSeconds = 0.5f;

    /*
    [Header("SoundStuff")]
    public AudioSource attackAudio;
    public AudioClip attackSFX;
    */

    [Header("Animation")]
    private Animator anim;

    GameManager gameMaster;

    private void Awake()
    {
        GetComponentInParent<CharacterStats>().OnHealthPercentChanged += HealthChanged; // Whenever "OnHealthPercentChanged", call"HealthChanged"
    }

    private void HealthChanged(float percent)                                           // To call start on a Coroutine
    {
        StartCoroutine(ChangeToPercent(percent));
    }

    private IEnumerator ChangeToPercent(float percent)                                  // Coroutine is to add lerp in hp reduction or otherwise by changing hp into a percentage
    {
        float beforeChangePercent = healthBar.fillAmount;
        float elapsed = 0f;                                                             // Amount of time passed

        while (elapsed < updateSpeedSeconds)                                            // Make sure time passed is lesser than update speed to run the while loop
        {
            elapsed += Time.deltaTime;
            healthBar.fillAmount = Mathf.Lerp(beforeChangePercent, percent, elapsed / updateSpeedSeconds);  // Update fill amount
            yield return null;
        }

        healthBar.fillAmount = percent;                                                 // Set at exact amount after lerp
    }

    // Start is called before the first frame update
    void Start()
    {
        gameMaster = FindObjectOfType<GameManager>();

        player = gameMaster.Player;                                              // Reference to player
        playerPos = player.transform;                                            // Reference to player location
        myStats = GetComponent<CharacterStats>();                                // Reference to stats
        combat = GetComponent<CharacterCombat>();                                // Reference to combat (attack)
        eneStats = GetComponent<EnemyStats>();                                   // Reference to Enemy's stats


        agent = GetComponent<NavMeshAgent>();                                    // Reference to NavMesh Agent

        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckPlayer();
    }

    // The radius where enemy detects player
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewRadius);                                         // Gizmos to be shown at Boss's position
    }

    void FaceTarget()
    {
        Vector3 direction = (playerPos.position - transform.position).normalized;                      // Get player's direction
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);  // Quaternion.Slerp is to add sotthing to rotation
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CharacterStats playerStats = player.GetComponent<CharacterStats>();                        // Gets reference to player's stats
            if (playerStats != null)
            {
                combat.Attack(playerStats);                                                            // Boss deals damage to player
            }
        }
    }

    // Boss ATTACK!!
    void AttackTarget()
    {
        anim.SetBool("Attack", true);                                                                  // Play Attack Animation

        SpawnRocks();
    }

    // Enemy Takes Damage from player
    public void TakeDamage()                                                                           // Called from player's weapon trigger
    {
        CharacterCombat playerCombat = player.GetComponent<CharacterCombat>();                         // Gets reference to player's damage  
        if (playerCombat != null)
        {
            playerCombat.Attack(myStats);                                                              // Player deals damage to enemy
            healthBar.fillAmount = myStats.currentHealth / myStats.maxHealth;
        }
    }

    void CheckPlayer()
    {
        if (player != null)
        {
            float distance = Vector3.Distance(playerPos.position, transform.position);  // Get distance between boss and player

            var targetCanvas = gameObject.transform.GetChild(1).gameObject;             // Reference to enemy's Canvas UI

            if (distance <= viewRadius)                                                 // Check if player is within range!
            {
                
                targetCanvas.SetActive(true);

                if (distance <= attackRadius)                                           // I'm right next to you... boss is within range to attack
                {
                    FaceTarget();
                    AttackTarget();
                }
                else
                {
                    anim.SetBool("Attack", false);
                }
            }
        }
    }

    void SpawnRocks()
    {
        spawnTime -= Time.deltaTime;
        if (spawnTime <= 0f)
        {
            rockSpawn = Instantiate(rockPrefab, spawnPoint5.transform.position, Quaternion.identity) as GameObject;
            spawnTime = 3f;

            rockSpawn = Instantiate(rockPrefab, spawnPoint6.transform.position, Quaternion.identity) as GameObject;
            spawnTime = 5f;

            rockSpawn = Instantiate(rockPrefab, spawnPoint7.transform.position, Quaternion.identity) as GameObject;
            spawnTime = 3f;

            rockSpawn = Instantiate(rockPrefab, spawnPoint8.transform.position, Quaternion.identity) as GameObject;
            spawnTime = 7f;

            rockSpawn = Instantiate(rockPrefab, spawnPoint9.transform.position, Quaternion.identity) as GameObject;
            spawnTime = 4f;

            Debug.Log(transform.name + " spawned rock.");
        }
    }
}
