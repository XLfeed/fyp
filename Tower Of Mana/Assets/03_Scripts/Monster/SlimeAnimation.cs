﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class SlimeAnimation : MonoBehaviour
{
    GameObject player;
    CharacterCombat combat;
    BoxCollider bc;

    void Start()
    {
        player = GameManager.instance.Player;
        combat = GetComponent<CharacterCombat>();                                // Reference to combat (attack)
        bc = gameObject.GetComponentInChildren<BoxCollider>();

    }
    void DealDamage()
    {
        bc.enabled = true;
        //combat.Attack(playerStats);                                                                // Enemy deals damage to player    
    }

    void StopDamage()
    {
        bc.enabled = false;
    }
}
