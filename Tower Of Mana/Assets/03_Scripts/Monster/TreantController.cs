﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class TreantController : EnemyController
{
    [Header("Sound Stuffy")]    
    AudioSource treantAttackAudio;
    public AudioClip treantAttackSFX;
    AudioSource takeDamageAttackAudio;
    public AudioClip takeDamageAttackSFX;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        
        treantAttackAudio = audioManager.GetComponentInChildren<AudioSource>();
        takeDamageAttackAudio = audioManager.transform.GetChild(9).GetComponent<AudioSource>();
    }

    // Enemy ATTACK!!
    //public override void AttackTarget()
    //{
    //    base.AttackTarget();                                                                           // Play Attack Animation
    //    CharacterStats playerStats = player.GetComponent<CharacterStats>();                            // Gets reference to player's stats

    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CollisionAttack();
            
        }
    }
    public override void PopUpDamage()
    {
        base.PopUpDamage();
        takeDamageAttackAudio.PlayOneShot(takeDamageAttackSFX);
    }

    public override void CollisionAttack()
    {
        base.CollisionAttack();
        treantAttackAudio.PlayOneShot(treantAttackSFX);                                                  // Play slime attck sound
    }
}
