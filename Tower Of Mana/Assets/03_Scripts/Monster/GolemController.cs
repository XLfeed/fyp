﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GolemController : EnemyController
{
    [Header("Sound Stuffy")]   
    
    AudioSource takeDamageAttackAudio;
    public AudioClip takeDamageAttackSFX;

    public override void Start()
    {
        base.Start();
        
        
        takeDamageAttackAudio = audioManager.transform.GetChild(9).GetComponent<AudioSource>();
    }

    // Enemy ATTACK!!
    //public override void AttackTarget()
    //{
    //    base.AttackTarget();                                                          // Play Attack Animation
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CollisionAttack();
        }
    }

    public override void PopUpDamage()
    {
        base.PopUpDamage();
        takeDamageAttackAudio.PlayOneShot(takeDamageAttackSFX);
    }

    public override void CollisionAttack()
    {
        base.CollisionAttack();
                                                     
    }
}
